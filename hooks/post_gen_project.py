import os
import shutil


VALID_DRIVERS = {
    'd': 'docker',
    'v': 'vagrant',
}
DEFAULT_DRIVER = '{{ cookiecutter.driver_name }}'


def ask_for_driver():
    response = None

    while response not in VALID_DRIVERS.keys():
        print()
        print("The available Molecule drivers are:")
        for driver_name in VALID_DRIVERS.values():
            first_letter = driver_name[0].upper()
            remaining_letters = driver_name[1:]
            print(f'  - [{first_letter}]{remaining_letters}')

        response = input('Please choose a driver (case insensitive): ').lower()

        if response not in VALID_DRIVERS.keys():
            print('Invalid choice!')
        else:
            return VALID_DRIVERS[response]


def customize_for_driver(driver_name):
    project_directory = os.getcwd()

    source = f'{project_directory}/molecule-templates/default-{driver_name}'
    destination = f'{project_directory}/molecule/default'
    shutil.copytree(source, destination)


if __name__ == "__main__":

    if DEFAULT_DRIVER in VALID_DRIVERS.keys():
        driver = VALID_DRIVERS[DEFAULT_DRIVER]
    elif DEFAULT_DRIVER in VALID_DRIVERS.values():
        driver = DEFAULT_DRIVER
    else:
        driver = ask_for_driver()

    customize_for_driver(driver)
