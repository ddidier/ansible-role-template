
# {{ cookiecutter.role_name }}

<!-- MarkdownTOC -->

1. [Requirements](#requirements)
1. [Role Variables](#role-variables)
    1. [Naming](#naming)
    1. [Variables in `defaults/main.yml`](#variables-in-defaultsmainyml)
    1. [Variables in `vars/main.yml`](#variables-in-varsmainyml)
    1. [External variables](#external-variables)
1. [Dependencies](#dependencies)
1. [Example Playbook](#example-playbook)
1. [Development](#development)
    1. [Best practices](#best-practices)
    1. [Test](#test)
        1. [Preparing tests](#preparing-tests)
        1. [Writing tests](#writing-tests)
        1. [Running tests](#running-tests)
1. [License](#license)
1. [Versions](#versions)
1. [References](#references)

<!-- /MarkdownTOC -->



<div class="placeholder">
    <p>
A brief description of the role goes here.
    </p>
</div>

{{ cookiecutter.role_description }}.



<a id="requirements"></a>
## Requirements

<div class="placeholder">
    <p>
Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.
    </p>
</div>



<a id="role-variables"></a>
## Role Variables

<div class="placeholder">
    <p>
A description of the settable variables for this role should go here, including any variables that are in `defaults/main.yml`, `vars/main.yml`, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. host vars, group vars, etc.) should be mentioned here as well.
    </p>
</div>

<a id="naming"></a>
### Naming

Since there is no namespace in Ansible, naming variables is important to avoid collision:

- variables are written in snake case.
- variables are prefixed with the playbook name acting as a namespace, ending with 2 underscores.

The pattern is then `<role_namespace>__<variable_name>`,
for example `{{cookiecutter.role_namespace}}__environment` (notice the 2 underscores).

<a id="variables-in-defaultsmainyml"></a>
### Variables in `defaults/main.yml`

Variables in this file ARE EASILY OVERRIDDEN (see [Ansible variable precedence] for more details).
If you are writing a redistributable role with reasonable defaults, put those in this file.
The role will bring along default values but ANYTHING in Ansible will override them.

| Name | Type | Default value | Description |
| ---- | ---- | ------------- | ----------- |
| {{ cookiecutter.role_namespace }}__http_port | integer | 80 | The port the HTTPd server listen on |

<a id="variables-in-varsmainyml"></a>
### Variables in `vars/main.yml`

Variables in this file ARE NOT EASILY OVERRIDDEN (see [Ansible variable precedence] for more details).
If you are writing a role and want to ensure the value in the role is absolutely used in that role,
and is not going to be overridden by inventory, you should put it in this file.

| Name | Type | Default value | Description |
| ---- | ---- | ------------- | ----------- |
| {{ cookiecutter.role_namespace }}__centos_configuration_file | string | /etc/httpd/conf/httpd.conf | The path of the configuration file on CentOS |
| {{ cookiecutter.role_namespace }}__debian_configuration_file | string | /usr/local/apache2/conf/httpd.conf | The path of the configuration file on Debian |

<a id="external-variables"></a>
### External variables

The following variables are mandatory but have no sensible defaults.
They are defined in `vars/main.yml` using the `{{ cookiecutter.role_namespace }}__mandatory_variable_names` variable.
They are not defined in the role: not in `defaults/` nor in `vars/`.
They must be defined elsewhere, for example in `group_vars/`.
The first task in `tasks/main.yml` is responsible for checking their existence.

| Name | Type | Description |
| ---- | ---- | ----------- |
| {{ cookiecutter.role_namespace }}__server_name | string | The name of the HTTPd server |



<a id="dependencies"></a>
## Dependencies

<div class="placeholder">
    <p>
A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.
    </p>
</div>



<a id="example-playbook"></a>
## Example Playbook

<div class="placeholder">
    <p>
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
    </p>

    <pre>
    - hosts: servers
      roles:
         - { role: {{ cookiecutter.role_name }}, x: 42 }
    </pre>
</div>



<a id="development"></a>
## Development

<a id="best-practices"></a>
### Best practices

You should follow some best practices, for example:

- [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
- [WhiteCloud Ansible Styleguide](https://github.com/whitecloud/ansible-styleguide)
- [Wiredcraft Ansible Template](https://github.com/Wiredcraft/ansible-template)
- [Some Ansible Best Practices](https://github.com/GSA/datagov-deploy/wiki/Best-Practices#ansible)
- [More Ansible Best Practices](https://github.com/enginyoyen/ansible-best-practises)

<div class="placeholder">
    <p>
A list of your own rules to follow and best practices.
    </p>
</div>

<a id="test"></a>
### Test

<a id="preparing-tests"></a>
#### Preparing tests

The testing framework is [Molecule].

The Python package manager is [PIP].

We strongly suggest that you use [pyenv].

Testing requires [Docker] and/or [Vagrant] with [VirtualBox] 6.0.

On Ubuntu 18.04:

<div class="placeholder">
    <p>
Change the GIT URL below.
    </p>
</div>

```bash
# install some useful dependencies
sudo apt install -y build-essential libssl-dev libffi-dev python-dev zlib1g-dev

# install PIP and VirtualEnv
sudo apt install -y python3-pip python3-venv

# install pyenv, and
# do not forget to read the installation output!
curl https://pyenv.run | bash

#
# again, do not forget to read the installation output!
#

# clone the template repository
git clone https://your.git.server/{{ cookiecutter.role_name }}.git
cd {{ cookiecutter.role_name }}

# create and activate the environments as given in the '.python-version' file
pyenv install 3.8.6
pyenv virtualenv 3.8.6 {{ cookiecutter.role_name }}_3.8.6
pyenv activate {{ cookiecutter.role_name }}_3.8.6
pip install --upgrade pip

# install all the required dependencies
pip install --requirement molecule/requirements.txt
```

<a id="writing-tests"></a>
#### Writing tests

Tests can be written in two different ways :

- using [pytest] framework
- using the [testinfra] framework (based on *pytest*)

The framework *testinfra* tests the features from the node being tested by Molecule.
The assertions are not very helpful IMHO, e.g. a service named `httpd` is running and listening on the port `80`.

The framework *pytest* tests the features from the host running Molecule.
You can use any library that you want to test a real business value, e.g. a web service is answering on the port `80`.

<a id="running-tests"></a>
#### Running tests

Test the role with the `molecule test` command that will run all the following stages:

```
└── default
    ├── lint
    ├── destroy
    ├── dependency
    ├── syntax
    ├── create
    ├── prepare
    ├── converge
    ├── idempotence
    ├── side_effect
    ├── verify
    └── destroy
```

If you want to develop and test incrementally, you may use these commands:

- `molecule test --destroy never`: run the tests and keep the containers or the VM.
- `molecule verify`: run the tests only.

Molecule will silence log output, unless invoked with the `--debug` flag, e.g.

- `MOLECULE_NO_LOG="false" molecule --debug test --destroy never`
- `MOLECULE_NO_LOG="false" molecule --debug verify`



<a id="license"></a>
## License

<div class="placeholder">
    <p>
        Do not forget to set a license on your work!
        The (same) license must be set in the following files:
    </p>
    <ul>
        <li>README.md (here)</li>
        <li>LICENSE.txt</li>
        <li>meta/main.yml</li>
    </ul>
</div>



<a id="versions"></a>
## Versions

This project has been generated using [NDD ansible-role-template] in the following state:

- generation date: `NDD_ROLE_TEMPLATE_GENERATION_DATE`
- commit hash: `NDD_ROLE_TEMPLATE_COMMIT_HASH`
- commit date: `NDD_ROLE_TEMPLATE_COMMIT_DATE`
- commit tags: `NDD_ROLE_TEMPLATE_COMMIT_TAGS`
- modified: `NDD_ROLE_TEMPLATE_MODIFIED`

These data may be used to incorporate later changes of the [NDD ansible-role-template] project into this project.

- generation date: when the current project has been generated based on the `ndd-role-template` project.
- commit hash: the head commit of the `ndd-role-template` project at generation time.
- commit date: the head commit of the `ndd-role-template` project at generation time.
- commit tags: the head commit of the `ndd-role-template` project at generation time.
- modified: True when there is uncommitted changes in the `ndd-role-template` project at generation time, False otherwise.



<a id="references"></a>
## References

[Ansible]: https://www.ansible.com/
[Ansible Galaxy syntax]: https://docs.ansible.com/ansible/latest/reference_appendices/galaxy.html#installing-multiple-roles-from-a-file
[Ansible variable precedence]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable
[CookieCutter]: https://github.com/audreyr/cookiecutter
[Docker]: https://www.docker.com/
[HTTPd]: https://httpd.apache.org/
[Molecule]: https://molecule.readthedocs.io/
[NDD ansible-role-template]: https://gitlab.com/ddidier/ansible-role-template
[PIP]: https://en.wikipedia.org/wiki/Pip_(package_manager)
[pyenv]: https://github.com/pyenv/pyenv
[pytest]: https://docs.pytest.org/
[testinfra]: https://testinfra.readthedocs.io/
[Vagrant]: https://www.vagrantup.com/
[VirtualBox]: https://www.virtualbox.org/
[VirtualEnv]: https://virtualenv.pypa.io/
[VirtualEnvWrapper]: https://virtualenvwrapper.readthedocs.io/

- [Ansible]
- [Ansible Galaxy syntax]
- [Ansible variable precedence]
- [CookieCutter]
- [Docker]
- [HTTPd]
- [Molecule]
- [PIP]
- [pyenv]
- [pytest]
- [testinfra]
- [Vagrant]
- [VirtualBox]
- [VirtualEnv]
- [VirtualEnvWrapper]



<style type="text/css">
    .placeholder::before {
        content: "TODO: ";
    }
    .placeholder {
        background-color: hsl(39,100%,75%);
        border: 1px solid hsl(39,100%,50%);
        font-style: italic;
        padding: 5px 10px;
    }
    .placeholder pre {
        background-color: hsl(39,100%,80%);
    }
    p + .placeholder {
        margin-top: 1rem;
    }
    .placeholder + p {
        margin-top: 1rem;
    }
</style>
