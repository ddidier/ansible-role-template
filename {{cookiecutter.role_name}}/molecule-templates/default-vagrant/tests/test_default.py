import os
import pytest
import requests
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', 'commons'))
from utilities import MoleculeHosts  # noqa: E402

molecule_hosts = MoleculeHosts()

# Get the hosts whose features are tested.
tested_hosts = molecule_hosts.vagrant_hosts_in('tested')

# Get the hosts from which the features are tested.
# Use this if you really need to have a container acting as a client,
# but it's way better to directly test from your host...
# testing_hosts = molecule_hosts.vagrant_hosts_in('testing')

# The tests are run against all hosts unless the test contains a 'testinfra_hosts' list.
# This list can contain hostnames or ansible groups.
testinfra_hosts = molecule_hosts.vagrant_host_names_in('tested')


# IMPORTANT: This Python script is executed on the host running Molecule.


# --------------------------------------------------------------- Internal -----
# Execute parts of the tests on the node being tested by Molecule.
# Very slow...

def test_httpd_service_is_running_and_enabled(host):
    # remotely executed on the tested node:
    if host.system_info.distribution == 'centos':
        service = host.service('httpd')
    elif host.system_info.distribution == 'debian':
        service = host.service('apache2')
    else:
        pytest.fail('Unsupported distribution: ' + host.system_info.distribution)
    assert service.is_running
    assert service.is_enabled


def test_httpd_service_is_listening(host):
    socket = host.socket("tcp://0.0.0.0:80")
    assert socket.is_listening


# --------------------------------------------------------------- External -----
# Execute the tests on the host running Molecule.
# Very fast!

@pytest.mark.parametrize("tested_host", tested_hosts)
def test_httpd_service_is_responding(tested_host):
    tested_host_url = f'http://{tested_host.ip}/'
    response = requests.get(tested_host_url)
    assert response.status_code == 200
    assert "Test Page" in response.text
