#! /bin/bash

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TEMPLATE_DIR=$(realpath "$SCRIPT_DIR/..")

BOLD_RED='\e[1;31m'
BOLD_GREEN='\e[1;32m'
BOLD_YELLOW='\e[1;33m'

TXT_RESET='\e[0m'

function test_main() {
    local driver_docker=n
    local driver_vagrant=n
    local debug=n
    local keep_generated_files=n

    # shellcheck disable=SC2034
    for i in "$@"; do
        case "$1" in
            --all)
                driver_docker=y
                driver_vagrant=y
                shift
                ;;
            --docker)
                driver_docker=y
                shift
                ;;
            --vagrant)
                driver_vagrant=y
                shift
                ;;
            --debug)
                debug=y
                shift
                ;;
            --keep-generated-files)
                keep_generated_files=y
                shift
                ;;
            -h|--help)
                test_help
                exit 0
                ;;
            *)
                test_help
                exit 1
                ;;
        esac
    done

    local driver_docker_result=0
    local driver_vagrant_result=0

    if [[ "$driver_docker" == "n" ]] && [[ "$driver_vagrant" == "n" ]]; then
        echo -e "At least one driver must be specified!"
        test_help
        exit 1
    fi

    if [[ "$driver_docker" == "y" ]]; then
        if ! test_driver "docker" "$keep_generated_files"; then
            driver_docker_result=1
        fi
    fi

    if [[ "$driver_vagrant" == "y" ]]; then
        if ! test_driver "vagrant" "$keep_generated_files"; then
            driver_vagrant_result=1
        fi
    fi

    echo -e "${BOLD_YELLOW}"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ Test results"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e "${TXT_RESET}"

    if [[ "$driver_docker" == "y" ]]; then
        if [[ $driver_docker_result -eq 0 ]]; then
            echo -e "${BOLD_GREEN} - Docker driver test: SUCCESS${TXT_RESET}"
        else
            echo -e "${BOLD_RED} - Docker driver test: FAILED${TXT_RESET}"
        fi
    else
        echo -e "${BOLD_YELLOW} - Docker driver test: NOT RUN${TXT_RESET}"
    fi

    if [[ "$driver_vagrant" == "y" ]]; then
        if [[ $driver_vagrant_result -eq 0 ]]; then
            echo -e "${BOLD_GREEN} - Vagrant driver test: SUCCESS${TXT_RESET}"
        else
            echo -e "${BOLD_RED} - Vagrant driver test: FAILED${TXT_RESET}"
        fi
    else
        echo -e "${BOLD_YELLOW} - Vagrant driver test: NOT RUN${TXT_RESET}"
    fi

    if [[ $driver_docker_result -eq 0 ]] && [[ $driver_vagrant_result -eq 0 ]]; then
        exit 0
    else
        exit 1
    fi
}

function test_help() {
    echo -e "Usage: test <DRIVERS...> <OPTIONS...>"
    echo -e "DRIVERS:"
    echo -e "   --all                   Test all drivers"
    echo -e "   --docker                Test Docker driver"
    echo -e "   --vagrant               Test Vagrant driver"
    echo -e "OPTIONS:"
    echo -e "   --debug                 Increase the output verbosity"
    echo -e "   --keep-generated-files  Do not delete the generated project files"
}

function test_driver() {
    local driver_name="$1"
    local keep_files="$2"
    local temp_dir

    temp_dir="$(mktemp -d --suffix=.ndd-role-template)"

    echo -e "${BOLD_YELLOW}"
    echo -e " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e " ┃ Testing with driver: ${driver_name}"
    echo -e " ┃ Testing in directory: ${temp_dir} (keep files = $keep_files)"
    echo -e " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    echo -e "${TXT_RESET}"

    # create a VirtualEnv for the role creation
    local template_venv_path="${temp_dir}/ndd-role-template-venv"

    virtualenv -p python3 "${template_venv_path}"
    # shellcheck disable=SC1090
    source "${template_venv_path}/bin/activate"
    pip3 install --requirement "${TEMPLATE_DIR}/requirements.txt"

    # create the role
    local project_name="ndd_role_test"
    local project_path="${temp_dir}/${project_name}"
    local project_venv_path="${project_path}-venv"
    # local new_role_arguments=""

    # if [[ "$debug" == "y" ]]; then
    #     new_playbook_arguments="--debug"
    # fi

    "${TEMPLATE_DIR}/bin/new-role" \
        --role-directory="$temp_dir" \
        --role-name="$project_name" \
        --role-namespace="nddrt" \
        --role-description="Test ndd-role-template" \
        --driver-name="$driver_name"
        # "$new_role_arguments"

    pushd "$project_path"

    # change some Ansible Galaxy properties to make Ansible Lint pass...
    sed -i -r 's/^  company: .+$/  company: Test/g' "${project_path}/meta/main.yml"
    sed -i -r 's/^  license: .+$/  license: MIT/g' "${project_path}/meta/main.yml"
    sed -i -r 's/^  platforms: .+$/  platforms: [{"name":"Debian"}]/g' "${project_path}/meta/main.yml"

    # create a VirtualEnv for the role execution
    virtualenv -p python3 "${project_venv_path}"
    # shellcheck disable=SC1090
    source "${project_venv_path}/bin/activate"
    pip3 install --requirement "${project_path}/molecule/requirements.txt"

    # test the role
    local molecule_arguments=""
    local molecule_result=1

    if [[ "$debug" == "y" ]]; then
        molecule_arguments="$molecule_arguments --debug"
    fi

    # shellcheck disable=SC2086
    if molecule $molecule_arguments test; then
        molecule_result=0
    fi

    popd

    # keep the generated file for inspection, or don't
    if [[ "$keep_files" == "y" ]]; then
        echo -e "Keeping generated files in ${temp_dir}"
    else
        rm -rf "$temp_dir"
    fi

    return $molecule_result
}

test_main "$@"
